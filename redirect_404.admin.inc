<?php

/**
 * @file
 * Administrative settings for the Redirect 404 module.
 */

/**
 * Form builder; administrative settings.
 */
function redirect_404_admin_settings($form, &$form_state) {
  $form = array();
  $form['paths'] = array(
    '#type' => 'fieldset',
    '#title' => t('Paths'),
  );
  $form['paths']['redirect_404_destination_host'] = array(
    '#title' => t('Destination host'),
    '#description' => t('When a 404 error occurs the path will be appended to this host and an HTTP request will be immediately sent to the URI. If this request has succeeded, a redirection will be performed. Leave it empty to disable this action temporarily.'),
    '#type' => 'textfield',
    '#field_prefix' => 'http://',
    '#default_value' => variable_get('redirect_404_destination_host'),
    '#size' => 40,
  );
  $form['paths']['redirect_404_site_404'] = array(
    '#type' => 'textfield',
    '#title' => t('404 (not found) page'),
    '#default_value' => variable_get('redirect_404_site_404'),
    '#description' => t('Path where users will be redirected to when the HTTP request sent to the destination host has failed. Redirect 404 can also mimic the default "page not found" output, leave it empty to fallback to that behavior.'),
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#size' => 40,
  );
  $form['redirection_notice'] = array(
    '#type' => 'fieldset',
    '#title' => t('Redirection notice'),
  );
  $form['redirection_notice']['redirect_404_display_redirection_notice'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display redirection notice'),
    '#description' => t('Informing the user about the redirection that is going to happen.'),
    '#default_value' => variable_get('redirect_404_display_redirection_notice', TRUE),
  );
  $visible = array(
    'visible' => array(
      ':input[name=redirect_404_display_redirection_notice]' => array('checked' => TRUE),
    ),
  );
  $form['redirection_notice']['redirect_404_redirection_notice_interval'] = array(
    '#type' => 'select',
    '#title' => t('Redirection notice interval'),
    '#options' => array(
      2 => t('!seconds seconds', array('!seconds' => 2)),
      3 => t('!seconds seconds', array('!seconds' => 3)),
      4 => t('!seconds seconds', array('!seconds' => 4)),
      5 => t('!seconds seconds', array('!seconds' => 5)),
      6 => t('!seconds seconds', array('!seconds' => 6)),
    ),
    '#default_value' => variable_get('redirect_404_redirection_notice_interval', REDIRECT_404_DEFAULT_REDIRECTION_NOTICE_INTERVAL),
    '#description' => t('Notice will be shown for the specified time before the redirection happens.'),
    '#states' => $visible,
    '#required' => TRUE,
  );
  $form['redirection_notice']['redirect_404_redirection_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page title'),
    '#description' => t('Title of page on which the redirection message appears.'),
    '#default_value' => variable_get('redirect_404_redirection_page_title', REDIRECT_404_DEFAULT_REDIRECTION_PAGE_TITLE),
    '#states' => $visible,
    '#required' => TRUE,
  );
  $form['redirection_notice']['redirect_404_redirection_notice_text_plural'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirection notice text (more than 1 seconds left)'),
    '#description' => t('Text will be shown as the redirection notice when there are more then 1 seconds left until redirecting. Use the @seconds token to insert the remaining seconds and @destination to display where the redirection will happen to.'),
    '#default_value' => variable_get('redirect_404_redirection_notice_text_plural', REDIRECT_404_DEFAULT_REDIRECTION_NOTICE_TEXT_PLURAL),
    '#size' => 100,
    '#states' => $visible,
    '#required' => TRUE,
  );
  $form['redirection_notice']['redirect_404_redirection_notice_text_singular'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirection notice text (1 second left)'),
    '#description' => t('Text will be shown as the redirection notice when there is exactly 1 second left until redirecting. Use the @destination token to display where the redirection will happen to.'),
    '#default_value' => variable_get('redirect_404_redirection_notice_text_singular', REDIRECT_404_DEFAULT_REDIRECTION_NOTICE_TEXT_SINGULAR),
    '#size' => 100,
    '#states' => $visible,
    '#required' => TRUE,
  );
  $text_formats = array();
  foreach (filter_formats() as $format_id => $format) {
    $text_formats[$format_id] = $format->name;
  }
  $form['redirection_notice']['redirect_404_redirection_notice_text_format'] = array(
    '#type' => 'select',
    '#title' => t('Text format'),
    '#description' => t('Text format to output redirection notice text.'),
    '#default_value' => variable_get('redirect_404_redirection_notice_text_format', filter_default_format()),
    '#options' => $text_formats,
    '#states' => $visible,
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Form validate handler; administrative settings.
 */
function redirect_404_admin_settings_validate($form, &$form_state) {
  // Validate 404 path.
  if (!empty($form_state['values']['redirect_404_site_404']) && !drupal_valid_path($form_state['values']['redirect_404_site_404'])) {
    form_set_error('redirect_404_site_404', t("The path '%path' is either invalid or you do not have access to it.", array('%path' => $form_state['values']['redirect_404_site_404'])));
  }
}
