(function ($) {

/**
 * Performs redirection after the specified number of seconds have spent and
 * indicates how much time is left in the meantime.
 */

/**
 * Attaching the redirect and time updating behavior.
 */
Drupal.behaviors.Redirect404 = {
  attach: function (context, settings) {
    var seconds = Drupal.settings.redirect404.interval;
    var interval = setInterval(function() {
      seconds--;
      if (seconds > 1) {
        // Update the number of seconds left.
        $('.redirect-404-seconds').html(seconds.toString());
      }
      else if (seconds == 1) {
        // Redirection notice text is different when there is only 1 second
        // left.
        $('.redirect-404-redirection-notice p').html(Drupal.settings.redirect404.messageSingular);
      }
      else {
        clearInterval(interval);
        // It's time for the redirection.
        window.location.replace(Drupal.settings.redirect404.destination);
      }
    }, 1000);
  }
};

})(jQuery);
